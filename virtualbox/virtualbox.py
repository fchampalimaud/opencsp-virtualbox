from pyforms 			import BaseWidget
from pyforms.Controls 	import ControlDir
import pyforms, os, shutil
os.system('taskset -p 0xff {0}'.format(os.getpid()) )



class VirtualBox(BaseWidget):
	
	def __init__(self):
		super(VirtualBox,self).__init__('VirtualBox')

		self._files   = ControlDir('Directory with files to import to the server')
		self._formset = ['_files']
		
	def execute(self):
		# Reset the output directory ################################
		if os.path.exists('output'):	  shutil.rmtree('output')
		if not os.path.exists('output'):  os.makedirs('output')
		#############################################################

		from netifaces import interfaces, ifaddresses, AF_INET

		host_ip = None
		for ifaceName in interfaces():
			addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
			if ifaceName in ['eth0','enp6s0','eno1']: 
				host_ip = addresses
				break

		if host_ip!=None:
	
			command = ['vboxwebsrv', '-H', str(host_ip[0])]
			
			print( self.executeCommand( command ) )


##################################################################################################################
##################################################################################################################
##################################################################################################################

if __name__ == "__main__":	 pyforms.startApp( VirtualBox )