# Virtual box (OpenCSP application)
Initialize the virtual box service on remote computers.

## How to use

- From the internal FChampalimaud network access to http://opencsp.champalimaud.pt .
- Press the green button "Log-in with Google" to log with your google account.

![Login](docs/imgs/login.png?raw=true "Screen")

- On the top menu, press the button "Applications", and select the application "Virtualbox".

![Applications list](docs/imgs/applications-list.png?raw=true "Screen")

- A new tab will be open.

![Applications list](docs/imgs/virtualbox-app.png?raw=true "Screen")

- On this new tab you can see a input box with the label "Directory with files to import to the server". This input box allow you to upload files to the server which is going to run the Virtual Box service. You can use it to import ISO files or OVA files necessary to instanciate your virtual machines.

![Applications list](docs/imgs/select-file.png?raw=true "Screen")

- Select the folder by clicking on the blue circle on the right of the table rows.
- After selecting the folder, run the application by pressing the green|white button "Schedule job".

![Applications list](docs/imgs/schedule-job.png?raw=true "Screen")

- Select the server and press "Send job to queue".
- And go to the link: http://opencsp.champalimaud.pt:8000.

- Login with the user admin and pass admin and you will see a VirtualBox administration interface. 
- On the left side, where it says "VirtualBox (Server X), Hosting - 5.0.18_Ubuntu", you should click and from the list you select your server.

![Applications list](docs/imgs/virtualbox-interface.png?raw=true "Screen")

- After you are ready to instanciate your Virtual Machine. (Ex: https://www.youtube.com/watch?v=xtF2ZlDtLt4)